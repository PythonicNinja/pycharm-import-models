from django import VERSION

if VERSION[0]+VERSION[1]*0.1 >= 1.7:
    import django
    from django.apps import apps
    django.setup()
    models = apps.get_models()
else:
    from django.db.models.loading import get_models
    models = get_models()
    
for m in models:
    exec "from %s import %s" % (m.__module__, m.__name__)